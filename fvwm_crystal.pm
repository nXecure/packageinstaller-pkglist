<?xml version="1.0"?>
<app>

<category>
Window Managers
</category>

<name>  
fvwm_crystal
</name>

<description>  
fvwm-crystal window manager
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
fvwm
fvwm-crystal
fvwm-icons
trayer
quodlibet
xterm
xscreensaver
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
fvwm
fvwm-crystal
fvwm-icons
trayer
quodlibet
xterm
xscreensaver
</uninstall_package_names>

</app>
