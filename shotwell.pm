<?xml version="1.0"?>
<app>

<category>
Graphics
</category>

<name>  
Shotwell
</name>

<description>  
Digital photo organizer
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
shotwell
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
shotwell
</uninstall_package_names>
</app>