<?xml version="1.0"?>
<app>

<category>
Graphics
</category>

<name>  
Zim
</name>

<description>  
Graphical text editor used to maintain a collection of wiki pages
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
zim
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
zim
</uninstall_package_names>
</app>