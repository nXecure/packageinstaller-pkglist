<?xml version="1.0"?>
<app>

<category>
FTP
</category>

<name>  
Filezilla
</name>

<description>  
Full-featured FTP client with an easy-to-use GUI
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
filezilla
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
filezilla
</uninstall_package_names>
</app>