<?xml version="1.0"?>
<app>

<category>
Video
</category>

<name>  
mplayer
</name>

<description>  
Powerful multimedia player and much more
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
mplayer
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
mplayer
</uninstall_package_names>
</app>