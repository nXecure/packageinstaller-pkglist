<?xml version="1.0"?>
<app>

<category>
Graphics
</category>

<name>  
Inkscape
</name>

<description>  
Vector-based drawing program
</description>

<installable>
all
</installable>

<screenshot>https://screenshots.debian.net/screenshots/000/015/295/large.png</screenshot>

<preinstall>

</preinstall>

<install_package_names>
inkscape
aspell
imagemagick
perlmagick
pstoedit
python-lxml
python-numpy
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
inkscape
</uninstall_package_names>
</app>