<?xml version="1.0"?>
<app>

<category>
Network
</category>

<name>
PIAVPN - Private Internet Access VPN
</name>

<description>
Private Internet Access VPN
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
piavpn-downloader-installer
</install_package_names>

<postinstall>

if [ -x /usr/share/pia-downloader-installer/install_piavpn.sh ]; then
/usr/share/pia-downloader-installer/install_piavpn.sh
fi

</postinstall>

<uninstall_package_names>
piavpn-downloader-installer
</uninstall_package_names>
</app>
