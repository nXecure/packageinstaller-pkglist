<?xml version="1.0"?>
<app>

<category>
Messaging
</category>

<name>
Zoom
</name>

<description>
Zoom Teleconference Client
</description>

<installable>
32
</installable>

<screenshot>none</screenshot>

<preinstall>
wget https://zoom.us/client/latest/zoom_i386.deb
</preinstall>

<install_package_names>
./zoom_i386.deb
</install_package_names>


<postinstall>
rm zoom_i386.deb
</postinstall>


<uninstall_package_names>
zoom
</uninstall_package_names>
</app>
