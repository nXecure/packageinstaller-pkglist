<?xml version="1.0"?>
<app>

<category>
Development
</category>

<name>  
Meld
</name>

<description>  
Graphical tool to diff and merge files
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
meld
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
meld
</uninstall_package_names>
</app>