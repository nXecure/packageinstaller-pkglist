<?xml version="1.0"?>
<app>

<category>
LaTeX
</category>

<name>  
LaTeX-Deutsche_Pakete
</name>

<description>  
Basic LaTeX-Installation
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
texlive-lang-german
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
texlive-lang-german
</uninstall_package_names>

</app>
