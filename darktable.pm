<?xml version="1.0"?>
<app>

<category>
Graphics
</category>

<name>  
Latest darktable
</name>

<description>  
Virtual lighttable and darkroom for photographers
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
darktable
liblensfun-bin 
</install_package_names>


<postinstall>
lensfun-update-data
</postinstall>


<uninstall_package_names>
darktable
liblensfun-bin 
</uninstall_package_names>
</app>