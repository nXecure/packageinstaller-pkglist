<?xml version="1.0"?>
<app>

<category>
Desktop Environment
</category>

<name>  
Xfce4-lite
</name>

<description>  
Very minimal install of Xfce4
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
xfce4
thunar-volman
thunar
gvfs
elogind
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
xfce4
thunar-volman
thunar
</uninstall_package_names>
</app>