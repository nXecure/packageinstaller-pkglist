<?xml version="1.0"?>
<app>

<category>
Games
</category>

<name>  
Simple Card Games
</name>

<description>  
Simple Desktop Card and Puzzle Games (solitare, hearts, mines, etc)
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gnome-games
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gnome-games
</uninstall_package_names>
</app>