<?xml version="1.0"?>
<app>

<category>
Utility
</category>

<name>
clonezilla
</name>

<description>
Clone and restore disks and partitions
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
clonezilla
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
clonezilla
</uninstall_package_names>
</app>
