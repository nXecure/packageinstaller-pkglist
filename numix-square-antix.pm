<?xml version="1.0"?>
<app>

<category>
Icons
</category>

<name>
Numix Square antiX
</name>

<description>
Numix Square icons for antiX Linux
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
numix-square-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
numix-square-antix
</uninstall_package_names>
</app>
