<?xml version="1.0"?>
<app>

<category>
Audio
</category>

<name>  
Pithos
</name>

<description>  
Native Pandora radio client
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
pithos
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
pithos
</uninstall_package_names>
</app>