<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
Chinese_TW_LO_Latest_full
</name>

<description>  
Chinese_traditional localisation of LibreOffice
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "deb http://deb.debian.org/debian buster-backports main contrib non-free">/etc/apt/sources.list.d/pitemp.list
apt-get update
</preinstall>

<install_package_names>
-t buster-backports libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-zh-tw
libreoffice-help-zh-tw
libreoffice-gtk3
</install_package_names>

<postinstall>
rm /etc/apt/sources.list.d/pitemp.list
apt-get update
</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-zh-tw
libreoffice-help-zh-tw
libreoffice-gtk3
</uninstall_package_names>

</app>
