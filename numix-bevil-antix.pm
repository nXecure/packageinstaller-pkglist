<?xml version="1.0"?>
<app>

<category>
Icons
</category>

<name>
Numix Bevel antiX
</name>

<description>
Numix Bevil icons for antiX Linux
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
numix-bevel-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
numix-bevel-antix
</uninstall_package_names>
</app>
