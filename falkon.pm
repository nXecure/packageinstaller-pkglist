<?xml version="1.0"?>
<app>

<category>
Browser
</category>

<name>
Falkon
</name>

<description>
Latest Falkon lightweight browser
</description>

<installable>
all
</installable>

<screenshot>https://screenshots.debian.net/screenshots/000/017/192/large.png</screenshot>

<preinstall>

</preinstall>

<install_package_names>
falkon
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
falkon
</uninstall_package_names>
</app>
