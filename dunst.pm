<?xml version="1.0"?>
<app>

<category>
Misc
</category>

<name>
Dunst
</name>

<description>
Highly configurable and lightweight notification-daemon
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
dunst
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
dunst
</uninstall_package_names>
</app>
