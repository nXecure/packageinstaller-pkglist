<?xml version="1.0"?>
<app>


<category>
Screencast
</category>

<name>  
SimpleScreenRecorder
</name>

<description>  
Qt-based screencast recorder
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
simplescreenrecorder
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
simplescreenrecorder
</uninstall_package_names>

</app>
