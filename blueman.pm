<?xml version="1.0"?>
<app>

<category>
Network
</category>

<name>  
Blueman
</name>

<description>  
GTK bluetooth management utility
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
blueman
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
blueman
</uninstall_package_names>
</app>