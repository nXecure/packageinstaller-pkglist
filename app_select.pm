<?xml version="1.0"?>
<app>

<category>
Utility
</category>

<name>  
App Select
</name>

<description>  
Quick applications search
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
app-select-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
app-select-antix
</uninstall_package_names>
</app>