<?xml version="1.0"?>
<app>

<category>
Icons
</category>

<name>
Papirus-Mini antiX
</name>

<description>
Basic set of Papirus icons for antiX Linux
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
papiris-mini-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
papiris-mini-antix
</uninstall_package_names>
</app>
