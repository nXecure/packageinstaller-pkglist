<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
Turkish_Firefox
</name>

<description>  
Turkish localisation of latest Firefox from Mozilla
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-l10n-tr
</install_package_names>

<postinstall>
</postinstall>

<uninstall_package_names>
firefox-l10n-tr
</uninstall_package_names>

</app>
