<?xml version="1.0"?>
<app>

<category>
Video
</category>

<name>  
Openshot
</name>

<description>  
Non-linear video editor
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
openshot
openshot-doc
frei0r-plugins
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
openshot
openshot-doc
frei0r-plugins
</uninstall_package_names>
</app>