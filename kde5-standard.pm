<?xml version="1.0"?>
<app>

<category>
Desktop Environment
</category>

<name>  
KDE5-standard
</name>

<description>  
Standard install of KDE5
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
kde-standard
kwin-x11
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
kde-standard
kwin-x11
</uninstall_package_names>
</app>