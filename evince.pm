<?xml version="1.0"?>
<app>

<category>
Office
</category>

<name>
Evince
</name>

<description>
Simple document (PostScript, PDF) viewer
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
evince
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
evince
</uninstall_package_names>
</app>
