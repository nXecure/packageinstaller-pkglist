<?xml version="1.0"?>
<app>

<category>
Audio
</category>

<name>  
XMMS
</name>

<description>  
Multimedia player modelled on winamp
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
xmms
xmms-skins-antix 
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
xmms
xmms-skins-antix 
</uninstall_package_names>
</app>