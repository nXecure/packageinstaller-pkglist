<?xml version="1.0"?>
<app>

<category>
Graphics
</category>

<name>  
CherryTree
</name>

<description>  
Hierarchical note taking application
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
cherrytree
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
cherrytree
</uninstall_package_names>
</app>