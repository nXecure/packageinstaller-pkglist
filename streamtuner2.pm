<?xml version="1.0"?>
<app>

<category>
Audio
</category>

<name>  
Streamtuner2
</name>

<description>  
Browser for radio station directories
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
streamtuner2
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
streamtuner2
</uninstall_package_names>
</app>