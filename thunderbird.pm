<?xml version="1.0"?>
<app>

<category>
Email
</category>

<name>  
thunderbird
</name>

<description>  
Latest Thunderbird email client 
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
thunderbird
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
thunderbird
</uninstall_package_names>
</app>