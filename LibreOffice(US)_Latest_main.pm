<?xml version="1.0"?>
<app>

<category>
Office
</category>

<name>  
Office-LibreOffice(US)_Latest_main
</name>

<description>  
Libre Office - a multiplatform and multilingual office suite (no database)
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "deb http://deb.debian.org/debian buster-backports main contrib non-free">/etc/apt/sources.list.d/pitemp.list
apt-get update
</preinstall>

<install_package_names>
-t buster-backports libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-en-us
libreoffice-gtk3
</install_package_names>

<postinstall>
rm /etc/apt/sources.list.d/pitemp.list
apt-get update
</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-en-us
libreoffice-gtk3
</uninstall_package_names>

</app>
