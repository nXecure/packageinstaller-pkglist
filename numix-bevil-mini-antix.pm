<?xml version="1.0"?>
<app>

<category>
Icons
</category>

<name>
Numix Bevel Mini antiX
</name>

<description>
Basic set of Numix Bevel icons for antiX Linux
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
numix-bevel-mini-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
numix-bevel-mini-antix
</uninstall_package_names>
</app>
