<?xml version="1.0"?>
<app>

<category>
Icons
</category>

<name>
Evolvere Icon Themes
</name>

<description>
evolvere icons (vibrant)
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
evolvere-icon-theme
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
evolvere-icon-theme
</uninstall_package_names>
</app>
