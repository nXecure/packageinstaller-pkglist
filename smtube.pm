<?xml version="1.0"?>
<app>

<category>
Video
</category>

<name>  
SMtube
</name>

<description>  
Search and download Youtube videos
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
smtube
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
smtube
</uninstall_package_names>
</app>