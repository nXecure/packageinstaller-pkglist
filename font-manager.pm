<?xml version="1.0"?>
<app>

<category>
Graphics
</category>

<name>
font-manager
</name>

<description>
Font management application from the GNOME desktop
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
font-manager
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
font-manager
</uninstall_package_names>
</app>
