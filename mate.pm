<?xml version="1.0"?>
<app>

<category>
Desktop Environment
</category>

<name>  
MATE
</name>

<description>  
Basic install of MATE desktop
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
mate-core
mate-desktop-environment
elogind
gvfs
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
mate-core
mate-desktop-environment
</uninstall_package_names>
</app>