<?xml version="1.0"?>
<app>

<category>
File Managers
</category>

<name>  
ROXFiler
</name>

<description>  
Fast and powerful graphical file manager
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
rox-filer
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
rox-filer
</uninstall_package_names>
</app>