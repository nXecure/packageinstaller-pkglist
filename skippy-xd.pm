<?xml version="1.0"?>
<app>

<category>
Utility
</category>

<name>  
Skippy-XD
</name>

<description>  
Full-screen task-switcher for X11.
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
skippy-xd
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
skippy-xd
</uninstall_package_names>
</app>