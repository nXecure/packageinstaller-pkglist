<?xml version="1.0"?>
<app>

<category>
Desktop Environment
</category>

<name>  
LXQT
</name>

<description>  
Basic install of LXQT
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
lxqt
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
lxqt
</uninstall_package_names>
</app>