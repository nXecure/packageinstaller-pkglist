<?xml version="1.0"?>
<app>

<category>
Video
</category>

<name>  
SMplayer
</name>

<description>  
Themable gui frontend to MPlayer with other interesting features
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
smplayer
smplayer-themes
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
smplayer
smplayer-themes
</uninstall_package_names>
</app>