<?xml version="1.0"?>
<app>

<category>
Misc
</category>

<name>
Winehq-staging
</name>

<description>
Run Windows applications without a copy of Microsoft Windows
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 276ECD5CEF864D8F
echo "deb http://mxrepo.com/mx/repo/ buster main non-free">/etc/apt/sources.list.d/pitemp1.list
sleep .1
apt-get update
</preinstall>

<install_package_names>
-t mx winehq-staging
</install_package_names>


<postinstall>
/usr/share/packageinstaller-pkglist/run_winecfg.sh
rm /etc/apt/sources.list.d/pitemp.list
rm /etc/apt/sources.list.d/pitemp1.list
apt-get update
</postinstall>


<uninstall_package_names>
winehq-staging
</uninstall_package_names>
</app>
