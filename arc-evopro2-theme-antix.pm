<?xml version="1.0"?>
<app>

<category>
Themes
</category>

<name>  
Arc-EvoPro2 Gtk Theme
</name>

<description>  
antiX 19 GTK theme (Dark)
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
arc-evopro2-theme-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
arc-evopro2-theme-antix
</uninstall_package_names>
</app>
