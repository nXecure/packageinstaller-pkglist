<?xml version="1.0"?>
<app>

<category>
Network
</category>

<name>  
wicd
</name>

<description>  
Lightweight general-purpose network configuration server 
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
wicd
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
wicd
</uninstall_package_names>
</app>