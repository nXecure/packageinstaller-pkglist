<?xml version="1.0"?>
<app>

<category>
Video
</category>

<name>  
Kdenlive
</name>

<description>  
KDE video editor
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
kdenlive
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
kdenlive
</uninstall_package_names>
</app>