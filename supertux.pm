<?xml version="1.0"?>
<app>

<category>
Games
</category>

<name>  
SuperTux
</name>

<description>  
Mario style platform game w/ Tux
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
supertux
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
supertux
</uninstall_package_names>
</app>