<?xml version="1.0"?>
<app>

<category>
Misc
</category>

<name>
Wine
</name>

<description>
Run Windows applications without a copy of Microsoft Windows
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
wine
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
wine
</uninstall_package_names>
</app>
