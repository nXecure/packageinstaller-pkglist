<?xml version="1.0"?>
<app>


<category>
Screencast
</category>

<name>
OBS-Studio
</name>

<description>
Streaming video and screencaster (OpenGL 3 hardware required)
</description>

<installable>
all
</installable>

<screenshot>https://screenshots.debian.net/screenshots/000/014/886/large.png</screenshot>

<preinstall>

</preinstall>

<install_package_names>
obs-studio
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
obs-studio
</uninstall_package_names>

</app>
