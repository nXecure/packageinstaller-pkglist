<?xml version="1.0"?>
<app>

<category>
Audio
</category>

<name>  
Qmmp
</name>

<description>  
Winamp/xmms type feature-rich audio player written in Qt
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
qmmp
qmmp-plugin-projectm 
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
qmmp
qmmp-plugin-projectm 
</uninstall_package_names>
</app>