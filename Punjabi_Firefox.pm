<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
Punjabi_Firefox
</name>

<description>  
Punjabi (India) localisation of latest Firefox from Mozilla
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-l10n-pa-in
</install_package_names>

<postinstall>
</postinstall>

<uninstall_package_names>
firefox-l10n-pa-in
</uninstall_package_names>

</app>
