<?xml version="1.0"?>
<app>

<category>
Misc
</category>

<name>  
Java
</name>

<description>  
Java 11 - installs openjdk-11
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
openjdk-11-jre
openjdk-11-jre-headless
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
openjdk-11-jre
openjdk-11-jre-headless
</uninstall_package_names>
</app>